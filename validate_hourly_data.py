from openpyxl import load_workbook
from pathlib import Path
import os
import datetime as dt


def round_datetime(d: dt.datetime) -> dt.datetime:
    """Round a datetime to the named precision.

    Cf. https://stackoverflow.com/a/3464000/7232335
    """
    d += dt.timedelta(**{f"hours": 1}) / 2
    d -= dt.timedelta(minutes=d.minute,
        seconds=d.second,
        microseconds=d.microsecond)
    return d


def validate(location, min_row=2, min_col=1, max_col=2):
    expected_difference = dt.timedelta(hours=1)
    last_time: dt.datetime|None = None
    problems: list[str] = []

    wb = load_workbook(location, data_only=True, read_only=True)
    ws = wb.worksheets[0]


    for row in ws.iter_rows(min_row=min_row, min_col=min_col, max_col=max_col):
        #check that row contains what it should
        if (row[0].value is None) or (row[1].value is None):
            continue
        
        local_time:dt.datetime = row[0].value           # type: ignore

        # NOTE: For some reason, the datetime can have some rounding issues not 
        #       visible in excel so this will round to nearest hour
        if row[0].value.minute != 0:                    # type: ignore
            local_time = round_datetime(local_time)     # type: ignore

        if last_time is None:
            last_time = local_time
            continue
        
        difference = local_time - last_time
        if difference != expected_difference:
            if local_time == last_time:
                problems.append(f"hour repeated at {local_time}")
            else:
                problems.append(f"hour potentially missing, goes from {last_time} to {local_time}")
        last_time = local_time
    if problems:
        print()
        print(f"Problems found in {location}")
        for problem in problems:
            print(problem)
        print()
    wb.close()

def validate_folder(folder):
    for filename in os.listdir(folder):
        file_path = folder / filename
        validate(file_path)

def main():
    main_path = Path().resolve()

    path = main_path / "Hourly/00-Provincial"
    for year in os.listdir(path):
        year_folder = path / year
        print(f"processing {year} folder")
        validate_folder(year_folder)
    print('Finished checking provincial demand')


    path = main_path / "Hourly/00-Interprovincial"
    for year in os.listdir(path):
        year_folder = path / year
        print(f"processing {year} folder")
        validate_folder(year_folder)
    print('Finished checking interprovincial transfers')


    path = main_path / "Hourly/00-International"
    for year in os.listdir(path):
        year_folder = path / year
        print(f"processing {year} folder")
        
        transfers_folder = year_folder / "Transfers"
        validate_folder(transfers_folder)

        prices_folder = year_folder / "Prices"
        validate_folder(prices_folder)

    print('Finished checking international transfers')


if __name__ == "__main__":
    main()